# Project Architecture

## Overview
This document describes the architecture of the Secure Kubernetes Deployments using ArgoCD. The project focuses on deploying a multi-tier application consisting of a frontend, backend, and database, each with enforced network policies to ensure secure communication and operation.

## Components
- **Frontend**: A simple web interface running on Nginx.
- **Backend**: A Node.js application providing RESTful API services.
- **Database**: A PostgreSQL database for persistent data storage.

## Infrastructure
- **Kubernetes**: Manages containerized applications across a cluster of machines.
- **ArgoCD**: Implements a GitOps approach to manage Kubernetes resources based on manifests stored in a Git repository.
- **Network Policies**: Implemented using Calico, ensuring strict network isolation between components.

## Diagram
```
+-------------+       +------------+       +------------+
|             | <---- |            | <---- |            |
|  Frontend   |       |  Backend   |       |  Database  |
|             | ----> |            | ----> |            |
+-------------+       +------------+       +------------+
```
Each component is isolated within its own subnet, with network policies restricting access to only necessary communication paths.

## Deployment Flow
- Changes to application configurations or versions are committed to the Git repository.
- ArgoCD watches the repository and automatically applies the changes to the Kubernetes cluster.
- Network policies are managed alongside application deployments to ensure security measures are always in sync with application states.
