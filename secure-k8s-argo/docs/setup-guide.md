# Setup Guide

## Prerequisites
- Kubernetes cluster (Minikube, Kind, or a cloud provider's Kubernetes service)
- kubectl configured to communicate with your Kubernetes cluster
- Helm 3 (optional for deploying some components)
- Access to a terminal and basic familiarity with command line tools and Git

## Installation Steps

### 1. Install ArgoCD
Refer to the installation manifest provided in the 'argocd/installations' directory:
```bash
kubectl apply -f secure-k8s-argo/argocd/installations/install.yaml
```

### 2. Set Up Applications in ArgoCD
Deploy each application component using the provided ArgoCD application manifests:
```bash
kubectl apply -f secure-k8s-argo/argocd/applications/frontend-app.yaml
kubectl apply -f secure-k8s-argo/argocd/applications/backend-app.yaml
kubectl apply -f secure-k8s-argo/argocd/applications/database-app.yaml
```

### 3. Verify Deployments
Check the status of the deployments using:
```bash
kubectl get pods --all-namespaces
```

## Accessing ArgoCD UI
- Forward the ArgoCD server port to your local machine:
```bash
kubectl port-forward svc/argocd-server -n argocd 8080:443
```
- Access the UI at `http://localhost:8080` and use the default credentials to log in.

## Troubleshooting
- Ensure all YAML files are correctly formatted and valid.
- Check pod logs and describe resources for more detailed error information.
