#!/bin/bash

NAMESPACE="myapp"

# Test Frontend to Backend connectivity
echo "Testing connectivity from Frontend to Backend..."
FRONTEND_POD=$(kubectl get pods -n $NAMESPACE -l app=frontend -o jsonpath="{.items[0].metadata.name}")
BACKEND_SERVICE_IP=$(kubectl get svc -n $NAMESPACE backend -o jsonpath="{.spec.clusterIP}")

if kubectl exec -n $NAMESPACE $FRONTEND_POD -- curl -s $BACKEND_SERVICE_IP:3000; then
  echo "Connectivity test from Frontend to Backend PASSED"
else
  echo "Connectivity test from Frontend to Backend FAILED"
fi

# Test Backend to Database connectivity
echo "Testing connectivity from Backend to Database..."
BACKEND_POD=$(kubectl get pods -n $NAMESPACE -l app=backend -o jsonpath="{.items[0].metadata.name}")
DATABASE_SERVICE_IP=$(kubectl get svc -n $NAMESPACE database -o jsonpath="{.spec.clusterIP}")

if kubectl exec -n $NAMESPACE $BACKEND_POD -- curl -s $DATABASE_SERVICE_IP:5432; then
  echo "Connectivity test from Backend to Database PASSED"
else
  echo "Connectivity test from Backend to Database FAILED"
fi
