#!/bin/bash

NAMESPACE="myapp"

# Check if Frontend is available
echo "Checking Frontend service availability..."
FRONTEND_IP=$(kubectl get svc -n $NAMESPACE frontend -o jsonpath="{.spec.clusterIP}")
if curl -s -o /dev/null -w "%{http_code}" http://$FRONTEND_IP:80 | grep 200; then
  echo "Frontend service is available and responding."
else
  echo "Frontend service is not available."
fi

# Check if Backend is available
echo "Checking Backend service availability..."
BACKEND_IP=$(kubectl get svc -n $NAMESPACE backend -o jsonpath="{.spec.clusterIP}")
if curl -s -o /dev/null -w "%{http_code}" http://$BACKEND_IP:3000 | grep 200; then
  echo "Backend service is available and responding."
else
  echo "Backend service is not available."
fi

# Check if Database service is responding (using a basic TCP check as HTTP might not be appropriate)
echo "Checking Database service availability..."
DATABASE_IP=$(kubectl get svc -n $NAMESPACE database -o jsonpath="{.spec.clusterIP}")
if nc -zv $DATABASE_IP 5432; then
  echo "Database service is available and accepting connections."
else
  echo "Database service is not available."
fi
