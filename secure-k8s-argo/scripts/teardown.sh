#!/bin/bash
# Define the namespace
NAMESPACE="myapp"
# Delete all resources in the namespace
kubectl delete all --all -n \$NAMESPACE
# Optionally delete the namespace itself
echo "Do you want to delete the namespace as well? (y/n)"
read DELETE_NS
if [ "\$DELETE_NS" == "y" ]; then
  kubectl delete namespace \$NAMESPACE
  echo "Namespace '\$NAMESPACE' and all its resources have been deleted."
else
  echo "All resources in namespace '\$NAMESPACE' have been deleted, but the namespace has been preserved."
fi
