#!/bin/bash
# Path to the Persistent Volume and Persistent Volume Claim configurations
PVC_DIR="secure-k8s-argo/kubernetes/pvcs"
# Create Persistent Volumes and Claims
kubectl apply -f \$PVC_DIR --namespace=myapp
echo "Persistent volumes and claims have been initialized."
