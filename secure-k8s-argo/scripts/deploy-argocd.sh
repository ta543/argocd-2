#!/bin/bash
# Path to the ArgoCD installation manifests
ARGOCD_DIR="secure-k8s-argo/argocd/installations"
# Install ArgoCD
kubectl apply -f \$ARGOCD_DIR/install.yaml
echo "ArgoCD has been successfully installed."
