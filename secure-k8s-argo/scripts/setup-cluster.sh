#!/bin/bash
# Define the base directory for the Kubernetes configurations
BASE_DIR="secure-k8s-argo/kubernetes"
# Create necessary namespaces
kubectl apply -f \$BASE_DIR/namespaces/myapp-namespace.yaml
# Apply network policies
kubectl apply -f \$BASE_DIR/network-policies/ --namespace=myapp
# Create ArgoCD applications
kubectl apply -f secure-k8s-argo/argocd/applications/ --namespace=argocd
echo "Kubernetes cluster and ArgoCD are set up."
