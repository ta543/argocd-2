# 🚀 Kubernetes Three-Tier Application Deployment with ArgoCD 🚀

This project demonstrates a three-tier application deployment on Kubernetes using ArgoCD for continuous delivery. The application is structured into three main components: a frontend served by Nginx, a backend API built with Node.js, and a PostgreSQL database. Network policies enforce strict communication rules between the tiers, ensuring security within the cluster.

## 📂 Project Structure

```
secure-k8s-argo/
├── argocd/
│   ├── applications/
│   │   ├── frontend-app.yaml
│   │   ├── backend-app.yaml
│   │   └── database-app.yaml
│   └── installations/
│       └── install.yaml
├── kubernetes/
│   ├── deployments/
│   │   ├── frontend-deployment.yaml
│   │   ├── backend-deployment.yaml
│   │   └── database-deployment.yaml
│   ├── services/
│   │   ├── frontend-service.yaml
│   │   ├── backend-service.yaml
│   │   └── database-service.yaml
│   ├── namespaces/
│   │   └── myapp-namespace.yaml
│   └── network-policies/
│       ├── default-deny.yaml
│       ├── frontend-policy.yaml
│       ├── backend-policy.yaml
│       └── database-policy.yaml
├── helm-charts/
│   ├── frontend/
│   ├── backend/
│   └── database/
├── docs/
│   ├── architecture.md
│   ├── setup-guide.md
│   └── security-report.md
├── tests/
│   ├── test-network-policies.sh
│   └── test-service-availability.sh
└── scripts/
    ├── setup-cluster.sh
    ├── deploy-argocd.sh
    ├── initialize-environment.sh
    └── teardown.sh
```

## 🛠️ Prerequisites

- Kubernetes cluster (e.g., Minikube, EKS, GKE)
- ArgoCD installed on your cluster
- kubectl configured to communicate with your cluster
- Helm (optional, if using Helm charts for deployment)

## 📦 Installation Steps

1. **Clone the Repository:**
   ```bash
   git clone https://github.com/yourusername/secure-k8s-argo.git
   cd secure-k8s-argo
   ```

2. **Set Up the Kubernetes Cluster:**
   Use the script to create namespaces, deployments, services, and apply network policies.
   ```bash
   ./scripts/setup-cluster.sh
   ```

3. **Deploy ArgoCD:**
   This script will install ArgoCD in your cluster if not already installed.
   ```bash
   ./scripts/deploy-argocd.sh
   ```

4. **Initialize Environment:**
   Run the initialization script to set up any required resources like PVCs.
   ```bash
   ./scripts/initialize-environment.sh
   ```

5. **Access ArgoCD UI:**
   Access ArgoCD to view the deployment status and manage applications directly from the UI.
   - Port-forward to access the ArgoCD dashboard:
     ```bash
     kubectl port-forward svc/argocd-server -n argocd 8080:443
     ```
   - Visit `http://localhost:8080` in your web browser.

6. **Verify Deployments:**
   Use the test scripts to ensure everything is functioning correctly.
   ```bash
   ./tests/test-network-policies.sh
   ./tests/test-service-availability.sh
   ```

## 📊 Managing the Deployment

- **Update Applications:**
  To update any application component, push the changes to your Git repository, and ArgoCD will automatically synchronize the changes to your cluster.

- **Teardown:**
  To remove all resources from your cluster:
  ```bash
  ./scripts/teardown.sh
  ```

## 📚 Documentation

Detailed documentation about the architecture, setup guide, and security considerations can be found in the `docs/` directory.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
